# README #

This is a slightly modified object that is based on the AES encryption class provided at http://aesencryption.net

Added ability to pad block and added a variable to set the IVKey when calling the class.

### Used for Military grade encryption ###

* Quick summary
* Version 1.0.3

### Example ###

```
#!php

/**
 * PKCS5 Padding String:
 *
 * In certain encryption cases, you may have the need to pad your string,
 * and in other cases you do not need to. This object will handle both
 * instances. If you need to pad your string, you have the ability to
 * do so by calling:
 *
 * AES::pkcs5_pad('STRING HERE');
 *
 * and you can even choose the blocksize for the pad. It is defaulted to
 * be set at 16.
 *
 * AES::instance()->pkcs5_pad('STRING HERE', 24);
 */

/**
 * Encryption String
 * @param type string
 */
$encString  = AES::instance()->pkcs5_pad('WG_167&WDBw*$rkly!09FQSS@vb8765!^^11111111^^2694');

/**
 * Encryption Key
 * @param type string
 */
$encKey     = 'fgh12@!jJK_H+88$5H8*23_328-76*&Q';

/**
 * Block Size
 *
 * 128      = MCRYPT_RIJNDAEL_128
 * 192      = MCRYPT_RIJNDAEL_192
 * 256      = MCRYPT_RIJNDAEL_256
 *
 * If set to null, default is 128
 *
 * @param type integer
 */
$blockSize  = 128;

/**
 * Encryption Mode
 *
 * cbc      = MCRYPT_MODE_CBC
 * cfb      = MCRYPT_MODE_CFB
 * ecb      = MCRYPT_MODE_ECB
 * nofb     = MCRYPT_MODE_NOFB
 * ofb      = MCRYPT_MODE_OFB
 * strream  = MCRYPT_MODE_STREAM
 *
 * If set to null, default is MCRYPT_MODE_CBC
 *
 * @param type string
 */
$encMode    = 'cbc';

/**
 * IV Key
 *
 * This is required when using CBC
 *
 * @param type string
 */
$ivKey      = 'g$K$H^28^7264(*^';

/**
 * Initialize new instance of the AES Encryption Object
 */
$aes        = new AES($encString, $encKey, $blockSize, $encMode, $ivKey);

/**
 * Encrypt Data
 */
$enc        = $aes->encrypt();

/**
 * Set encrypted string for decryption
 */
$aes->setData($enc);

/**
 * Decrypt Data
 *
 * May need to PKCS5 Un-Pad data if using padding
 * If not, just use decrypt
 * $aes->decrypt()
 */
$dec        = $aes->pkcs5_unpad($aes->decrypt());

echo "After encryption: ".$enc."<br/>";
echo "After decryption: ".$dec."<br/>";
```

### Contribution guidelines ###

* Create new fork and enjoy

### Changes ###
* v1.0.0 - Initial creation
* v1.0.1 - Added unpad functionality
* v1.0.2 - Added default mode if one isn't set
* v1.0.3 - Made new public static instance() function to instantiate AES class and made pkcs5_pad and pkcs5_unpad public functions so that padding can be added before calling the AES class with the data to be encrypted

